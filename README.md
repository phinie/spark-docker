# Run you own little Spark-Cluster!

## Start Master:

	docker run -d -p 8080:8080 -p 7077:7077 --name master phimar/spark-master
	
## Run and connect slaves:

	docker run -d --link master:master phimar/spark-slave
	
## Run interactive shell

	docker run -it --rm --link master:master phimar/spark-shell
	
## Using docker-compose:
	master:
		image: phimar/spark-master
		ports:
		- "8080:8080"
		- "7077:7077"
	slave:
		image: phimar/spark-slave
		links:
		- master:master

## Scale slaves:

	docker-compose scale slave=5
	